#include <Ultrasonic.h>
#include <SPI.h>
#include <MFRC522.h>

//******Módulo RFID*************************************************
/* Conexões:
 * Signal       Pin                 
 * ------------------------------
 * Reset        9                
 * SPI SAD      10              
 * SPI MOSI     11              
 * SPI MISO     12             
 * SPI SCK      13               
 */
//Declaração das constantes referentes aos pinos digitais.

#define SS_PIN 10
#define RST_PIN 9
#define BUZ_PIN 17
#define buffer_size 2
#define button_PIN 14
/*buffer_size é o numero máximo de elementos cadastrados*/

MFRC522 mfrc522(SS_PIN, RST_PIN); // Create MFRC522 instance.

//Declrações de variáveis

typedef struct{
  int tamanho;
  byte id[10];
}Cards;

/*Buffer de Tags*/
Cards tags[buffer_size];

/*Numero de Tags Cadastradas*/
int num_tags;
int lastID;/*Guarda o ultimo indice verificado com sucesso*/
int test = 0;
int var = 0;


//*******Módulo Detecção de Objetos***************************************

//criando objeto ultrasonic e definindo as portas digitais 
//do Trigger - 2, 4 - e Echo - 7, 5
Ultrasonic ultrasonic1(2,7);
Ultrasonic ultrasonic2(4,5);

//Declaração das constantes referentes aos pinos digitais.
const int dist1 = 20;
const int dist2 = 30;
const int dist3 = 11;
const int vibPin = 3;
const int vibPin2 = 6;
int i;
// Declaração das variáveis extras
long microsec1 = 0;
long microsec2 = 0;
float distanciaCM1 = 0;
float distanciaCM2 = 0;

//**********Módulo Integração******************************************************
//Declaração das constantes referentes aos pinos digitais.
const int chavePin = 16;
const int botaoModo_Pin = 15;

// Declaração das variáveis
int modo = 0;
int chave;
int botaoModo;


void setup() {
  Serial.begin(9600); //Inicializando o serial monitor
  SPI.begin();      // Init SPI bus
  mfrc522.PCD_Init(); // Init MFRC522 card
  
  //Definindo pinos digitais
//************Módulo RFID**********************
 Serial.println("Scan PICC to see UID and type...");
  pinMode(BUZ_PIN,OUTPUT);
  pinMode(button_PIN,INPUT);

//************Módulo Detecção de Objetos************
  pinMode(vibPin, OUTPUT); 
  pinMode(vibPin2, OUTPUT);
//***********Módulo Integração**********************
  pinMode(chavePin, INPUT);
  pinMode(botaoModo_Pin, INPUT);


}

void buzzer(int times){
/*times possui a qnt de vezes que o buzzer deve apitar*/
  for(int i = 0; i != times; i++){
    digitalWrite(BUZ_PIN, HIGH);  
    delay(100);              
    digitalWrite(BUZ_PIN, LOW);    
    delay(250);               
  }
}

void erro(){
    digitalWrite(BUZ_PIN, HIGH);  
    delay(500);
    digitalWrite(BUZ_PIN, LOW);
}

int getID(){
/*
  Recolhe informações da tag.
*/
  //Procura por tag
  if ( ! mfrc522.PICC_IsNewCardPresent()) {
    return 0;
  }

  // Seleciona uma tag
  if ( ! mfrc522.PICC_ReadCardSerial()) {
    return 0;
  }
  
  // Dump debug info about the card. PICC_HaltA() is automatically called.
  mfrc522.PICC_DumpToSerial(&(mfrc522.uid));
  return 1;
}

boolean lerTag(){
  int time3;
  int time2;
  boolean leitura;
  time3 = millis();
  
  /*Tentativa de Ler a Tag por 3 segundos*/
  do{
    Serial.print("leitura\n");
    leitura = getID();
    time2 = millis();
  }while(!leitura && ((time2 - time3)< 3000));
      
  return leitura;
}

boolean checkID(){
  int count;
  int i;
  boolean controle;

  controle = false;
  /*Percorrer o vetor com as Tags cadastradas*/
  for(count = 0; (count < num_tags) && (!controle); count++){
    
    /*Verifica se os tamanhos são compatíveis*/
    /*Se não forem as tags já são diferentes*/
    if(tags[count].tamanho == mfrc522.uid.size){
      controle = true;
      /*
      *Re-uso da variavel de controle
      *
      * Assumindo que as tags são iguais até que se encontre uma diferença
      * Se as Tags forem Iguais até o fim do vetor de IDs 
      * o controle será true no fim do laço saindo do laço superior.
      * Caso contrario procurasse na próxima Tag cadastrada.
      */
      /*Pecorre o vetor de IDs de cada Tag*/
      for(i=0; (i < mfrc522.uid.size)&&(controle) ;i++){
        if(tags[count].id[i] != mfrc522.uid.uidByte[i]){
          controle = false;
        }
        
      }
    }
  }
  if(controle){
    lastID = count;
   }
   
  /*Caso controle continue false nenhuma a Tag em questão não esta cadastrada*/ 
  return controle;
}

void adicionarTag(){
  int i;
  boolean idctr;
  
  //idctr = lerTag();
  
  /*if(!idctr){ERRO NA LEITURA
    buzzer(2);
  }else */
  
  if  ((num_tags +1) == buffer_size){/*ARMAZENAMENTO CHEIO*/
    erro();
    //buzzer(2);
  }else{
    /*Checa se a Tag já não esta cadastrada*/
    if(!checkID()){
    
      /*Armazena o tamanho da nova Tag*/
      tags[num_tags].tamanho = mfrc522.uid.size; 
      /*Armazena a ID da nova Tag*/
      for(i=0; i < mfrc522.uid.size ;i++){
        tags[num_tags].id[i] = mfrc522.uid.uidByte[i];
      }
      
      /*incrementa o numero de elementos*/
      num_tags = num_tags +1;
      
      buzzer(3);
      /*
      * Nota:
      * Caso de Uso necessita ser Atualizado
      */
    }
  }
}

int verificarTag(){
  boolean ctr;
  int retorno;
    
  ctr = lerTag();
  if(ctr){
    ctr = checkID();
    if(ctr){
      buzzer(1);
      retorno = 0;
    }else{
      buzzer(2);
      retorno = 1;
    }
  }else{
   erro();
   retorno = 2;
  }
  return retorno;
}

void botao2(){
  botaoModo = digitalRead(botaoModo_Pin);
  if(botaoModo == HIGH)
  {
     Serial.print("2 botão, modo modificado");
     delay(500);
     modo = 1;
   }
}



void loop() {
    chave = digitalRead(chavePin);
    int estado = 0;
    int botao;
    int time2;
    int time3;
    
    if(chave == HIGH)
    { 
      
      if(modo == 0)
      {
            microsec1 = ultrasonic1.timing();
            microsec2 = ultrasonic2.timing(); 
            distanciaCM1 = ultrasonic1.convert(microsec1, Ultrasonic::CM);
            distanciaCM2 = ultrasonic2.convert(microsec2, Ultrasonic::CM);
           
          
            Serial.println("ultra 1: ");
            Serial.print(distanciaCM1);
            Serial.println(" cm");
            Serial.println("ultra 2: ");
            Serial.print(distanciaCM2);
            Serial.println(" cm");
            analogWrite(vibPin,0);
            analogWrite(vibPin2,0);

            //Pra mudar para modo RFID 
            botaoModo = digitalRead(botaoModo_Pin);
            if(botaoModo == HIGH)
            {
              Serial.print("2 botão, modo modificado");
              delay(200);
              modo = 1;
            }
            
            //começo do while
            
            if((distanciaCM1 <= dist1)&& (distanciaCM2 <= dist1)&& (modo == 0)) //A em 1 B em 1
            {
               analogWrite(vibPin,255);
               analogWrite(vibPin2,255);
               delay(200);
               digitalWrite(vibPin,0);
               analogWrite(vibPin2,0);
               delay(400);
            }
            if((distanciaCM1 <= dist1) &&((distanciaCM2 <= dist2) && (distanciaCM2 > dist1))&&(modo == 0) ) //A em 1 B em 2
            { 
               for(i = 0; i < 2; i++) 
               {   
                 analogWrite(vibPin,255);
                 delay(200);
                 digitalWrite(vibPin,0);
                 delay(400);
                }
               analogWrite(vibPin2,255);
               delay(200);
               analogWrite(vibPin2,0);
               delay(800); 
              // microsec1 = ultrasonic1.timing();    
               //distanciaCM1 = ultrasonic1.convert(microsec1, Ultrasonic::CM);        
            }

            if((distanciaCM1 <= dist1) && (distanciaCM2 > dist2)&&(modo == 0)) //A em 1 e B em 3
            {       
               analogWrite(vibPin,255);
               delay(200);
               analogWrite(vibPin,0);
               delay(400);     
            }

             if(((distanciaCM1 <= dist2) && (distanciaCM1 > dist1)) && (distanciaCM2 <= dist1)&&(modo == 0)) //A em 2 e B  em 1
            { 
               for(i = 0; i < 2; i++) 
               {
                 analogWrite(vibPin2,255);
                 delay(200);
                 analogWrite(vibPin2,0);
                 delay(400); 
               }     
               analogWrite(vibPin,255);
               delay(200);
               analogWrite(vibPin,0);
               delay(800);
            }

            if(((distanciaCM1 <= dist2) && (distanciaCM1 > dist1)) && ((distanciaCM2 <= dist2) && (distanciaCM2 > dist1))&&(modo == 0)) // A em 2 e B em 2
            {
               analogWrite(vibPin,255);
               analogWrite(vibPin2,255);
               delay(200);
               digitalWrite(vibPin,0);
               analogWrite(vibPin2,0);
               delay(800); 
              
            }            botao2();
            if(((distanciaCM1 <= dist2) && (distanciaCM1 > dist1)) && (distanciaCM2 > dist2)&&(modo == 0))// A em 2 E b em 3
            {
               analogWrite(vibPin,255);
               delay(200);
               analogWrite(vibPin,0);
               delay(400);
            }

            if((distanciaCM1 > dist2) && (distanciaCM2 <= 20) ) // A em 3 e B 1
            {  
               analogWrite(vibPin2,255);
               delay(200);
               analogWrite(vibPin2,0);
               delay(400);
              
            }

            if((distanciaCM1 > dist2) && (distanciaCM2 < 30) && (distanciaCM2 > 20)&&(modo == 0) ) //A em 3 e B em 2
            {
               analogWrite(vibPin2,255);
               delay(200);
               analogWrite(vibPin2,0);
               delay(800); 
            }
            
            analogWrite(vibPin, 0);
            analogWrite(vibPin2, 0);
        
        
          
      }
      if(modo == 1)
      {
          Serial.print("MODO RFID/n");
            if(estado == 0)
            {
              botao = digitalRead(button_PIN);
              Serial.print("Estado 0 \n");
              Serial.print("numero tag :\n");
              Serial.print(num_tags);
              Serial.print("\n");
               Serial.print("VAR:\n");
              Serial.print(var);
              Serial.print("\n");
              if(botao==HIGH || var == 2 )
              {
                Serial.print("BOTAO\n");
                Serial.print(num_tags);
                botao = LOW;
                delay(200);
                var = verificarTag();
                 Serial.print("VAR:\n");
                 Serial.print(var);
                 Serial.print("\n");
                if(var == 1)
                {
                  Serial.print("Muda o estado\n");
                  estado = 1; // var == 1
                  Serial.print("VAR:\n");
                  Serial.print(var);
                  Serial.print("\n");
                }
                      
              }
            }  
            if((estado == 1))// var == 1
            {
              Serial.print("Estado 1 \n");
              Serial.print("Espera confirmar cadastro\n");    
              time2 = millis();
              do{
                  Serial.print("Espera confirmar cadastramento\n");
                  botao = digitalRead(button_PIN);
                  if(botao==HIGH)
                  {
                      Serial.print("BOTAO2\n");
                      Serial.print("estado cadastrar\n");
                      //botao = LOW;
                      delay(200);
                      adicionarTag();
                      estado = 0;
                   }
                  time3 = millis();
              }while(estado==1 && ((time3 - time2) < 6000));
              Serial.print("numero tag :");
              Serial.print(num_tags);
              Serial.print("\n");
              var = 0;
              estado = 0;              
             }             
          
          botaoModo = digitalRead(botaoModo_Pin);
          if(botaoModo == HIGH)
          {
            Serial.print("2 botão, modo modificado");
            delay(200);
            modo = 0;
            
          }
        }
      }
      else
      {
        Serial.print("DESLIGADO \n");
        modo = 0;
      }
        
    }
