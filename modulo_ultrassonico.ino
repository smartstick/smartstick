  
#include <Ultrasonic.h>
//criando objeto ultrasonic e definindo as portas digitais 
//do Trigger - 6 - e Echo - 7
Ultrasonic ultrasonic1(6,7);
Ultrasonic ultrasonic2(4,5);
 
//Declaração das constantes referentes aos pinos digitais.
const int ledVerde = 13;
const int ledAmarelo = 12;
const int ledVermelho = 11;
const int vibPin = 9;
 
long microsec1 = 0;
long microsec2 = 0;
float distanciaCM1 = 0;
float distanciaCM2 = 0;

void setup() {
 Serial.begin(9600); //Inicializando o serial monitor
   
  //Definindo pinos digitais

  pinMode(vibPin, OUTPUT); // 8 como saida.

}

void loop() {
 //Lendo o sensor
  microsec1 = ultrasonic1.timing();
  microsec2 = ultrasonic2.timing(); 
  distanciaCM1 = ultrasonic1.convert(microsec1, Ultrasonic::CM);
  distanciaCM2 = ultrasonic2.convert(microsec2, Ultrasonic::CM);
 

  Serial.println("ultra 1: ");
  Serial.print(distanciaCM1);
  Serial.println(" cm");
  Serial.println("ultra 2: ");
  Serial.print(distanciaCM2);
  Serial.println(" cm");
  analogWrite(vibPin, 0);
  
  //começo do while
  while((distanciaCM1 <= 20)||(distanciaCM2 <= 20))
  {       
     analogWrite(vibPin, 255);
     delay(200);
     analogWrite(vibPin,0);
     delay(400);
     microsec1 = ultrasonic1.timing();
     microsec2 = ultrasonic2.timing(); 
     distanciaCM1 = ultrasonic1.convert(microsec1, Ultrasonic::CM);
     distanciaCM2 = ultrasonic2.convert(microsec2, Ultrasonic::CM);    
  }
    while(((distanciaCM1 < 30) && (distanciaCM1 > 20))||((distanciaCM2 < 30) && distanciaCM2 > 20))
  {       
     analogWrite(vibPin, 255);
     delay(200);
     analogWrite(vibPin,0);
     delay(800);
     microsec1 = ultrasonic1.timing();
     microsec2 = ultrasonic2.timing(); 
     distanciaCM1 = ultrasonic1.convert(microsec1, Ultrasonic::CM);
     distanciaCM2 = ultrasonic2.convert(microsec2, Ultrasonic::CM);    
  }
  analogWrite(vibPin, 0);
}

